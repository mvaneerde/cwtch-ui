{
    "@@locale": "pl",
    "@@last_modified": "2021-07-14T23:49:07+02:00",
    "showMessageButton": "Show Message",
    "blockedMessageMessage": "This message is from a profile you have blocked.",
    "placeholderEnterMessage": "Type a message...",
    "plainProfileDescription": "We recommend that you protect your Cwtch profiles with a password. If you do not set a password on this profile then anyone who has access to this device may be able to access information about this profile, including contacts, messages and sensitive cryptographic keys.",
    "encryptedProfileDescription": "Encrypting a profile with a password protects it from other people who may also use this device. Encrypted profiles cannot be decrypted, displayed or accessed until the correct password is entered to unlock them.",
    "addContactConfirm": "Add contact %1",
    "addContact": "Add contact",
    "contactGoto": "Go to conversation with %1",
    "settingUIColumnOptionSame": "Same as portrait mode setting",
    "settingUIColumnDouble14Ratio": "Double (1:4)",
    "settingUIColumnDouble12Ratio": "Double (1:2)",
    "settingUIColumnSingle": "Single",
    "settingUIColumnLandscape": "UI Columns in Landscape Mode",
    "settingUIColumnPortrait": "UI Columns in Portrait Mode",
    "localePl": "Polish",
    "tooltipRemoveThisQuotedMessage": "Remove quoted message.",
    "tooltipReplyToThisMessage": "Reply to this message",
    "tooltipRejectContactRequest": "Reject this contact request",
    "tooltipAcceptContactRequest": "Accept this contact request.",
    "notificationNewMessageFromGroup": "New message in a group!",
    "notificationNewMessageFromPeer": "New message from a contact!",
    "tooltipHidePassword": "Hide Password",
    "tooltipShowPassword": "Show Password",
    "serverNotSynced": "Syncing New Messages (This can take some time)...",
    "groupInviteSettingsWarning": "You have been invited to join a group! Please enable the Group Chat Experiment in Settings to view this Invitation.",
    "shutdownCwtchAction": "Shutdown Cwtch",
    "shutdownCwtchDialog": "Are you sure you want to shutdown Cwtch? This will close all connections, and exit the application.",
    "shutdownCwtchDialogTitle": "Shutdown Cwtch?",
    "shutdownCwtchTooltip": "Shutdown Cwtch",
    "malformedMessage": "Malformed message",
    "profileDeleteSuccess": "Successfully deleted profile",
    "debugLog": "Turn on console debug logging",
    "torNetworkStatus": "Tor network status",
    "addContactFirst": "Add or pick a contact to begin chatting.",
    "createProfileToBegin": "Please create or unlock a profile to begin",
    "nickChangeSuccess": "Profile nickname changed successfully",
    "addServerFirst": "You need to add a server before you can create a group",
    "deleteProfileSuccess": "Successfully deleted profile",
    "sendInvite": "Send a contact or group invite",
    "sendMessage": "Send Message",
    "cancel": "Cancel",
    "resetTor": "Reset",
    "torStatus": "Tor Status",
    "torVersion": "Tor Version",
    "sendAnInvitation": "You sent an invitation for: ",
    "contactSuggestion": "This is a contact suggestion for: ",
    "rejected": "Rejected!",
    "accepted": "Accepted!",
    "chatHistoryDefault": "This conversation will be deleted when Cwtch is closed! Message history can be enabled per-conversation via the Settings menu in the upper right.",
    "newPassword": "New Password",
    "yesLeave": "Yes, Leave This Conversation",
    "reallyLeaveThisGroupPrompt": "Are you sure you want to leave this conversation? All messages and attributes will be deleted.",
    "leaveGroup": "Leave This Conversation",
    "inviteToGroup": "You have been invited to join a group:",
    "pasteAddressToAddContact": "Paste a cwtch address, invitation or key bundle here to add a new conversation",
    "tooltipAddContact": "Add a new contact or conversation",
    "titleManageContacts": "Conversations",
    "titleManageServers": "Manage Servers",
    "dateNever": "Never",
    "dateLastYear": "Last Year",
    "dateYesterday": "Yesterday",
    "dateLastMonth": "Last Month",
    "dateRightNow": "Right Now",
    "successfullAddedContact": "Successfully added ",
    "descriptionBlockUnknownConnections": "If turned on, this option will automatically close connections from Cwtch users that have not been added to your contact list.",
    "descriptionExperimentsGroups": "The group experiment allows Cwtch to connect with untrusted server infrastructure to facilitate communication with more than one contact.",
    "descriptionExperiments": "Cwtch experiments are optional, opt-in features that add additional functionality to Cwtch that may have different privacy considerations than traditional 1:1 metadata resistant chat e.g. group chat, bot integration etc.",
    "titleManageProfiles": "Manage Cwtch Profiles",
    "tooltipUnlockProfiles": "Unlock encrypted profiles by entering their password.",
    "tooltipOpenSettings": "Open the settings pane",
    "invalidImportString": "Invalid import string",
    "contactAlreadyExists": "Contact Already Exists",
    "conversationSettings": "Conversation Settings",
    "enterCurrentPasswordForDelete": "Please enter current password to delete this profile.",
    "enableGroups": "Enable Group Chat",
    "experimentsEnabled": "Enable Experiments",
    "localeIt": "Italiana",
    "localeEs": "Espanol",
    "addListItem": "Add a New List Item",
    "addNewItem": "Add a new item to the list",
    "todoPlaceholder": "Todo...",
    "newConnectionPaneTitle": "New Connection",
    "networkStatusOnline": "Online",
    "networkStatusConnecting": "Connecting to network and peers...",
    "networkStatusAttemptingTor": "Attempting to connect to Tor network",
    "networkStatusDisconnected": "Disconnected from the internet, check your connection",
    "viewGroupMembershipTooltip": "View Group Membership",
    "loadingTor": "Loading tor...",
    "smallTextLabel": "Small",
    "defaultScalingText": "Default size text (scale factor:",
    "builddate": "Built on: %2",
    "version": "Version %1",
    "versionTor": "Version %1 with tor %2",
    "themeDark": "Dark",
    "themeLight": "Light",
    "settingTheme": "Theme",
    "largeTextLabel": "Large",
    "settingInterfaceZoom": "Zoom level",
    "localeDe": "Deutsche",
    "localePt": "Portuguesa",
    "localeFr": "Frances",
    "localeEn": "English",
    "settingLanguage": "Language",
    "blockUnknownLabel": "Block Unknown Peers",
    "zoomLabel": "Interface zoom (mostly affects text and button sizes)",
    "versionBuilddate": "Version: %1 Built on: %2",
    "cwtchSettingsTitle": "Cwtch Settings",
    "unlock": "Unlock",
    "yourServers": "Your Servers",
    "yourProfiles": "Your Profiles",
    "error0ProfilesLoadedForPassword": "0 profiles loaded with that password",
    "password": "Password",
    "enterProfilePassword": "Enter a password to view your profiles",
    "addNewProfileBtn": "Add new profile",
    "deleteConfirmText": "DELETE",
    "deleteProfileConfirmBtn": "Really Delete Profile",
    "deleteConfirmLabel": "Type DELETE to confirm",
    "deleteProfileBtn": "Delete Profile",
    "passwordChangeError": "Error changing password: Supplied password rejected",
    "passwordErrorMatch": "Passwords do not match",
    "saveProfileBtn": "Save Profile",
    "createProfileBtn": "Create Profile",
    "passwordErrorEmpty": "Password cannot be empty",
    "password2Label": "Reenter password",
    "password1Label": "Password",
    "currentPasswordLabel": "Current Password",
    "yourDisplayName": "Your Display Name",
    "profileOnionLabel": "Send this address to peers you want to connect with",
    "noPasswordWarning": "Not using a password on this account means that all data stored locally will not be encrypted",
    "radioNoPassword": "Unencrypted (No password)",
    "radioUsePassword": "Password",
    "copiedToClipboardNotification": "Copied to Clipboard",
    "copyBtn": "Copy",
    "editProfile": "Edit Profille",
    "newProfile": "New Profile",
    "defaultProfileName": "Alice",
    "profileName": "Display name",
    "editProfileTitle": "Edit Profile",
    "addProfileTitle": "Add new profile",
    "deleteBtn": "Delete",
    "unblockBtn": "Unblock Peer",
    "dontSavePeerHistory": "Delete Peer History",
    "savePeerHistoryDescription": "Determines whether or not to delete any history associated with the peer.",
    "savePeerHistory": "Save Peer History",
    "blockBtn": "Block Peer",
    "saveBtn": "Save",
    "displayNameLabel": "Display Name",
    "addressLabel": "Address",
    "puzzleGameBtn": "Puzzle Game",
    "bulletinsBtn": "Bulletins",
    "listsBtn": "Lists",
    "chatBtn": "Chat",
    "rejectGroupBtn": "Reject",
    "acceptGroupBtn": "Accept",
    "acceptGroupInviteLabel": "Do you want to accept the invitation to",
    "newGroupBtn": "Create new group",
    "copiedClipboardNotification": "Copied to clipboard",
    "peerOfflineMessage": "Peer is offline, messages can't be delivered right now",
    "peerBlockedMessage": "Peer is blocked",
    "pendingLabel": "Pending",
    "acknowledgedLabel": "Acknowledged",
    "couldNotSendMsgError": "Could not send this message",
    "dmTooltip": "Click to DM",
    "membershipDescription": "Below is a list of users who have sent messages to the group. This list may not reflect all users who have access to the group.",
    "addListItemBtn": "Add Item",
    "peerNotOnline": "Peer is Offline. Applications cannot be used right now.",
    "searchList": "Search List",
    "update": "Update",
    "inviteBtn": "Invite",
    "inviteToGroupLabel": "Invite to group",
    "groupNameLabel": "Group name",
    "viewServerInfo": "Server Info",
    "serverSynced": "Synced",
    "serverConnectivityDisconnected": "Server Disconnected",
    "serverConnectivityConnected": "Server Connected",
    "serverInfo": "Server Information",
    "invitationLabel": "Invitation",
    "serverLabel": "Server",
    "search": "Search...",
    "cycleColoursDesktop": "Click to cycle colours.\nRight-click to reset.",
    "cycleColoursAndroid": "Click to cycle colours.\nLong-press to reset.",
    "cycleMorphsDesktop": "Click to cycle morphs.\nRight-click to reset.",
    "cycleMorphsAndroid": "Click to cycle morphs.\nLong-press to reset.",
    "cycleCatsDesktop": "Click to cycle category.\nRight-click to reset.",
    "cycleCatsAndroid": "Click to cycle category.\nLong-press to reset.",
    "blocked": "Blocked",
    "titlePlaceholder": "title...",
    "postNewBulletinLabel": "Post new bulletin",
    "newBulletinLabel": "New Bulletin",
    "joinGroup": "Join group",
    "createGroup": "Create group",
    "addPeer": "Add Peer",
    "groupAddr": "Address",
    "invitation": "Invitation",
    "server": "Server",
    "groupName": "Group name",
    "peerName": "Name",
    "peerAddress": "Address",
    "joinGroupTab": "Join a group",
    "createGroupTab": "Create a group",
    "addPeerTab": "Add a peer",
    "createGroupBtn": "Create",
    "defaultGroupName": "Awesome Group",
    "createGroupTitle": "Create Group"
}